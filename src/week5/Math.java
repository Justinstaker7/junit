//This class contains math methods that I refer to in my JUnit tests
//I might not use every method, I just want them available in case I change/need them for a test
//

package week5;

public class Math {

    //addition method
    public int add(int a, int b){

        return a + b;
    }

    //subtraction method
    public int sub(int a, int b){

        return a - b;
    }

    //multiplication method
    public int mul(int a, int b){

        return a * b;
    }

    //division method
    public int div(int a, int b){

        return a / b;
    }

}
