//Create a program that uses at least 5 of the different assert methods in a real-world scenario.
//had help from an two awesome youtube videos
//Link to youtube video: https://www.youtube.com/watch?v=C1jf1ARGM0g
//Link to youtube video: https://www.youtube.com/watch?v=o5pE7L2tVV8
//

package week5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class Main {

    public static void main(String[] args) {

        //tell user what program does
        System.out.println("This program runs 5 different tests using 5 different Assertions.");
    }

    //reference Math class to be used in the Tests
    Math doMath = new Math();

    //first test using addition from Math class
    @Test
    @DisplayName("AssertEquals")
    void test1() {
        System.out.println("--Testing using assertEquals assertion--");
        Assertions.assertEquals(5, doMath.add(3, 5), "testing for assertEquals");
    }

    //second test creating arrays to test if the arrays are equal
    @Test
    @DisplayName("AssertArrayEquals")
    void test2(){
        System.out.println("--Testing using assertArrayEquals assertion--");
        Assertions.assertArrayEquals(new int[] {1,2,3}, new int[] {1,2,3}, "testing for assertArrayEquals");
    }

    //third test testing if a boolean value is true
    @Test
    @DisplayName("AssertTrue")
    void test3(){
        System.out.println("--Testing using assertTrue assertion--");
        boolean trueValue = true;
        boolean falseValue = false;
        Assertions.assertTrue(trueValue, "Test for assertTrue");
    }

    //fourth test testing if a boolean value is false
    @Test
    @DisplayName("AssertFalse")
    void test4(){
        System.out.println("--Testing using assertFalse assertion--");
        boolean trueValue = true;
        boolean falseValue = false;
        Assertions.assertFalse(trueValue, "test for assertFalse");
    }

    //fifth test testing if an exception can handle a divide by zero
    @Test
    @DisplayName("AssertException")
    void test5(){
        System.out.println("--Testing using assertException assertion--");
        Assertions.assertThrows(ArithmeticException.class,
                () -> doMath.div(1,0), "test for assertException");
    }

    //output to user that the program is now finished
    public void outputEnd(){
        System.out.println("End of Program");
    }
}